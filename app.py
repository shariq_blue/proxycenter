from flask import Flask,render_template,request,session,redirect,flash,jsonify
from flask_sqlalchemy import SQLAlchemy
import json
from sqlalchemy.exc import IntegrityError as dupil
import requests
from requests.auth import HTTPBasicAuth


app = Flask(__name__)
app.secret_key='secret'
app.config['SQLALCHEMY_DATABASE_URI'] ="mysql+pymysql://phpmyadmin:panther33@localhost/proxycenter?charset=utf8mb4"
db=SQLAlchemy(app)
proxyid='rpcenter'
proxypass='KB21rZD7cR'

class Users(db.Model):
    __tablename__='users'
    username = db.Column(db.String(30), primary_key=True)
    password = db.Column(db.String(30), nullable=False)

class Ipauth(db.Model):
    __tablename__='ipauth'
    username = db.Column(db.String(30),primary_key=True)
    ipaddress = db.Column(db.String(20), nullable=False,primary_key=True)

class Proxylist(db.Model):
    __tablename__='proxy_list'
    proxy=db.Column(db.String(50))
    location = db.Column(db.String(50),primary_key=True)
    ports = db.Column(db.String(50))
    type = db.Column(db.String(50),primary_key=True)


@app.route('/login',methods=['GET','POST'])
def hello():
    if (request.method == 'GET'):
        if('user' in session):
            return redirect("/")
        else:
            return render_template("login.html")
    else:
        _email_=request.form.get('email')
        _password_=request.form.get('password')
        print(_email_)
        print(_password_)
        user = Users.query.filter_by(username=_email_).first()
        if (user is not None):
            if (_email_ == user.username and _password_ == user.password):
                session['user'] = _email_
                print(session['user'] + " user is loged in now")
                return redirect("/")
            else:
                return render_template("login.html")
        else:
            print("didiwork")
            return render_template("login.html")


@app.route('/')
def fashboard():
    if('user' in session):
        if (session['user'] == "root"):
            url = "https://residential-api.oxylabs.io/v1/login"
            response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
            auth = json.loads(response.text)
            url = "https://residential-api.oxylabs.io/v1/users/" + auth["user_id"] + "/sub-users"
            headers = {'authorization': 'Bearer ' + auth['token']}
            response = requests.request("GET", url, headers=headers)
            print(json.dumps(response.text))
            traffic_list=[]
            for user in json.loads(response.text):
                url="https://residential-api.oxylabs.io/v1/users/"+auth["user_id"]+"/sub-users/"+str(user["id"])+"?type=month"
                headers = {'authorization': 'Bearer ' + auth['token']}
                newresponse = requests.request("GET", url, headers=headers)
                print(newresponse.text)
                traffic_list.append(json.loads(newresponse.text))
            print(json.dumps(traffic_list))
            if ("usercrud" in session):
                if (session["usercrud"] == "success"):
                    flash("User created successfully", category="success")

                if (session["usercrud"] == "fail"):
                    flash("Failed to create user", category="danger")

                if (session["usercrud"] == "delete_success"):
                    flash("User Deleted successfully", category="success")

                if (session["usercrud"] == "delete_fail"):
                    flash("Failed to Delete user", category="danger")

                if (session["usercrud"] == "update_success"):
                    flash("User Updated successfully", category="success")

                if (session["usercrud"] == "update_fail"):
                    flash("Failed to Update user", category="danger")

                session.pop('usercrud')
                return render_template("getusers.html", users=json.loads(response.text),
                                       traffic=json.loads(json.dumps(traffic_list)))
            #print(json.loads(user_list))
            return render_template("getusers.html", users=json.loads(response.text),traffic=json.loads(json.dumps(traffic_list)))
        proxys=Proxylist.query.all()
        return render_template("createproxy.html",proxys=proxys)
    else:
        return redirect("/login")

@app.route("/changepass",methods=['POST','GET'])
def changepass():
    if (request.method == 'GET'):
        return render_template("changepass.html")
    elif(request.method=='POST'):
        oldpassword = request.form['oldpassword']
        password = request.form['password1']
        if('user' in session and session["user"] != "root"):
            user = Users.query.filter_by(username=session['user']).first()
            if (user is not None):
                if (oldpassword == user.password):
                    db.session.query(Users).filter_by(username=session['user']).update(
                        {"password": password})
                    db.session.commit()
                    flash("Password updated successfully", category="success")
                    return render_template("/changepass.html")
                else:
                    flash("Invalid password please try again", category="danger")
                    return render_template("/changepass.html")


@app.route('/users',methods=['GET'])
def users():
        if('user' in session):
            url = "https://api.smartproxy.com/v1/auth"
            response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
            auth = json.loads(response.text)
            url = "https://api.smartproxy.com/v1/users/" + auth["user_id"] + "/sub-users"
            headers = {'authorization': 'Token ' + auth['token']}
            response = requests.request("GET", url, headers=headers)
            print(json.dumps(response.text))
            if ("usercrud" in session):
                if (session["usercrud"] == "success"):
                    flash("User created successfully", category="success")

                if (session["usercrud"] == "fail"):
                    flash("Failed to create user", category="danger")

                if (session["usercrud"] == "delete_success"):
                    flash("User Deleted successfully", category="success")

                if (session["usercrud"] == "delete_fail"):
                    flash("Failed to Delete user", category="danger")

                session.pop('usercrud')
                return render_template("getusers.html", users=json.loads((response.text)))
            return render_template("getusers.html", users=json.loads((response.text)))
        else:
            return redirect("/")


@app.route("/updatesubuser",methods=['POST'])
def updateusers():
        id = request.form['id']
        username=request.form['username']
        password = request.form['password']
        traffic = request.form['traffic_limit']
        if(password!=""):
            print("got exexurtet")
            db.session.query(Users).filter_by(username=username).update(
                {"password": password})
            try:
                url = "https://residential-api.oxylabs.io/v1/login"
                response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
                auth = json.loads(response.text)
                url = "https://residential-api.oxylabs.io/v1/users/" + auth["user_id"] + "/sub-users/" + id
                payload = "{\"password\":\"" + password + "\",\"traffic_limit\":\"" + traffic + "\",\"lifetime\":\"false\"}"
                headers = {
                    'content-type': "application/json",
                    'authorization': "Bearer " + auth['token']
                }
                response = requests.request("PATCH", url, data=payload, headers=headers)
                print(response.text, "responsehere")
                if (response.text != "{}"):
                    raise Exception("invalid data")

            except:
                db.session.rollback()
                session["usercrud"] = "update_fail"
                return redirect("/")
            else:
                db.session.commit()
                session["usercrud"] = "update_success"
                return redirect("/")
        else:
            url = "https://residential-api.oxylabs.io/v1/login"
            response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
            auth = json.loads(response.text)
            url = "https://residential-api.oxylabs.io/v1/users/" + auth["user_id"] + "/sub-users/" + id
            payload = "{\"traffic_limit\":\"" + traffic + "\",\"lifetime\":\"false\"}"
            headers = {
                'content-type': "application/json",
                'authorization': "Bearer " + auth['token']
            }
            response = requests.request("PATCH", url, data=payload, headers=headers)
            print(response.text, "responsehere")
            if (response.text != "{}"):
                session["usercrud"] = "update_fail"
                return redirect("/")
            else:
                session["usercrud"] = "update_success"
                return redirect("/")






@app.route("/logout")
def logout():
    session.pop('user')
    return redirect("/")


@app.route("/deletesubuser",methods=["POST"])
def deletesubuser():
    id = request.form['id']
    username=request.form['username']
    Users.query.filter_by(username=username).delete()
    try:
        url = "https://residential-api.oxylabs.io/v1/login"
        response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
        auth = json.loads(response.text)
        url = "https://residential-api.oxylabs.io/v1/users/" + auth["user_id"] + "/sub-users/"+id
        headers = {'authorization': 'Bearer ' + auth['token']}
        response = requests.request("DELETE", url, headers=headers)
        print(response.text)
        if (response.text != ""):
                raise Exception("invalid data")
    except:
        db.session.rollback()
        session["usercrud"] = "delete_fail"
        return redirect("/")
    else:
        db.session.commit()
        session["usercrud"]="delete_success"
        return redirect("/")


@app.route("/getstats",methods=['GET'])
def getstats():
    if('user' in session):
        url = "https://residential-api.oxylabs.io/v1/login"
        response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
        auth = json.loads(response.text)
        url = "https://residential-api.oxylabs.io/v1/users/" + auth["user_id"] + "/sub-users"
        headers = {'authorization': 'Bearer ' + auth['token']}
        response = requests.request("GET", url, headers=headers)
        print(json.dumps(response.text))
        for user in json.loads(response.text):
            if(user["username"]==session['user']):
                url = "https://residential-api.oxylabs.io/v1/users/" + auth["user_id"] + "/sub-users/" + str(
                    user["id"]) + "?type=month"
                headers = {'authorization': 'Bearer ' + auth['token']}
                response2 = requests.request("GET", url, headers=headers)
                print(response2.text)
                stats=json.loads(response2.text)
                return json.dumps({'traffic':stats["traffic"],'trafficlimit':user["traffic_limit"]})
    else:
        return "Not authorised"



@app.route("/createuserscript",methods=['POST'])
def createuserscript():
    print(request.json["username"])
    if(request.json["usernameproxy"]==proxyid and request.json["passwordproxy"]==proxypass):
        username = request.json["username"]
        password = request.json['password']
        traffic = request.json['traffic']
        entry = Users(username=username, password=password)
        db.session.add(entry)
        try:
            url = "https://residential-api.oxylabs.io/v1/login"
            response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
            auth = json.loads(response.text)
            url = "https://residential-api.oxylabs.io/v1/users/" + auth["user_id"] + "/sub-users"
            payload = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\",\"traffic_limit\":" + traffic + ",\"life_time\":\"false\"}"
            print(url)
            headers = {
                'content-type': "application/json",
                'authorization': "Bearer " + auth['token']
            }
            response = requests.request("POST", url, data=payload, headers=headers)
            jsonresponse = json.loads(response.text)
            print(jsonresponse)
            if ("id" not in jsonresponse):
                raise Exception("invalid data")
        except dupil:
            db.session.rollback()
            return json.dumps({'status': "dupil"})

        except dupil:
            db.session.rollback()
            return json.dumps({'status': "fail"})

        else:
            db.session.commit()
            return json.dumps({'status': "success"})
    else:
        return json.dumps({'status':"denied"})



@app.route('/createuser',methods=['GET','POST'])
def createuser():
    if (request.method == 'GET'):
        return render_template("createuser.html")
    else:
        username=request.form["username"]
        password = request.form['password']
        traffic = request.form['traffic']
        entry = Users(username=username, password=password)
        db.session.add(entry)
        try:
            url = "https://residential-api.oxylabs.io/v1/login"
            response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
            auth = json.loads(response.text)
            url = "https://residential-api.oxylabs.io/v1/users/" + auth["user_id"] + "/sub-users"
            payload = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\",\"traffic_limit\":" + traffic + ",\"life_time\":\"false\"}"
            print(url)
            headers = {
                'content-type': "application/json",
                'authorization': "Bearer " + auth['token']
            }
            response = requests.request("POST", url, data=payload, headers=headers)
            jsonresponse = json.loads(response.text)
            print(jsonresponse)
            if ("id" not in jsonresponse):
                raise Exception("invalid data")
        except dupil:
            db.session.rollback()
            return json.dumps({'status':"dupil"})
        except:
            db.session.rollback()
            session["usercrud"] = "fail"
            return redirect("/")
        else:
            db.session.commit()
            session["usercrud"]="success"
            return redirect("/")



@app.route("/auth")
def auth():
    url = "https://api.smartproxy.com/v1/auth"
    response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
    auth = json.loads(response.text)
    session['token']=auth["token"]
    print(auth)
    return auth

@app.route("/createproxy",methods=['POST'])
def createproxy():
    type = request.json["type"]
    location = request.json['location']
    url = "https://api.smartproxy.com/v1/auth"
    response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
    auth = json.loads(response.text)
    url = "https://api.smartproxy.com/v1/endpoints/"+type
    headers = {'authorization': 'Token ' + auth['token']}
    response = requests.request("GET", url, headers=headers)
    print(json.loads(response.text))
    for i in json.loads(response.text):
            if(i["location"]==location):
                print(i)
                return json.dumps(i)
    return json.dumps({'invaluid':'invalid'})

@app.route("/subusers")
def subusers():
    url = "https://api.smartproxy.com/v1/auth"
    response = requests.request("POST", url, auth=HTTPBasicAuth(proxyid, proxypass))
    auth = json.loads(response.text)
    url = "https://api.smartproxy.com/v1/users/" + auth["user_id"] + "/sub-users"
    headers = {'authorization': 'Token '+auth['token']}
    response = requests.request("GET", url, headers=headers)
    print(json.dumps(response.text))
    return render_template("crud.html",students=json.loads((response.text)))

@app.route("/test")
def testy():
    url = "https://api.smartproxy.com/v1/endpoints/sticky"
    headers = {'authorization': 'Token '+session['token']}
    response = requests.request("GET", url, headers=headers)
    #print(json.loads(response.text))
    for i in json.loads(response.text):
            if(i["location"]=="Random"):
                print(i)
    return "succr"

if __name__ == '__main__':
    app.run()